Patterns (from GoF)
===================

Creational patterns // Порождающие шаблоны
------------------------------------------
* Abstract Factory // Абстрактная фабрика
* Builder // Строитель
* Factory Method // Фабричный метод
* Prototype // Прототип
* Singleton // Одиночка

Structural patterns // Структурные шаблоны
------------------------------------------
* Adapter // Адаптер
* Bridge // Мост
* Composite // Компоновщик
* Decorator // Декоратор
* Facade // Фасад
* Flyweight // Приспособленец
* Proxy // Заместитель

Behavioural patterns // Поведенческие шаблоны
---------------------------------------------
* Chain of responsibility // Цепочка обязанностей
* Command // Команда 
* Interpreter // Интерпретатор
* Iterator // Итератор
* Mediator // Посредник
* Memento // Хранитель
* Observer // Наблюдатель
* State // Состояние
* Strategy // Стратегия
* Template Method // Шаблонный метод
* Visitor // Посетитель
